#### Any merge request or issue reported not following these guidelines will be ignored.

## Submitting Issues

Issues must be submitted in the following format or they will be **ignored**.

Copy & Paste the following template when submitting an issue.

Keep the titles short and to the point.

```
`Title`: `< Issue Title >`

`Category`: `< Bug | Feature | Misc >`

`Description`:

    < Any information regarding the issue here. >
    < Be as detailed as possible. >
    
```

## Merge-Request Guidelines

* Avoid overly-complex code. Follow the [KISS](https://en.wikipedia.org/wiki/KISS_principle) guidelines.
* Ensure your code meets the project's current code-style.
* Test your changes personally before requesting them to be committed.
* Don't commit changes that simply refractor existing code or its code style.
* Use NodeJS modules supported by BetterDiscord in preference to writing your own methods.
* Comment each component of your changes using block quotes and be as detailed as possible. ( `/* ... */` )
* General internal methods should begin with two underscores ( `__` ) and be declared as static. ( If applicable. )